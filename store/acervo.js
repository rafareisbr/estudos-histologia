export default {
  state: () => ({
    menu: [
      {
        name: 'Epitelial',
        isCategoria: true,
        children: [
          {
            name: 'B1',
            categoria: 'Epitelial',
          },
          {
            name: 'B2',
            categoria: 'Epitelial',
          },
          {
            name: 'B3',
            categoria: 'Epitelial',
          },
        ],
      },
    ],
    items: [
      {
        name: 'B1',
        cards: [
          {
            name: 'B1 - Mesentério (Prata)',
            image: '/imagens/epitelial/b01-02.jpg',
            obs: null,
          },
          {
            name: 'Epitélio nitratado (células pavimentosas)',
            image: '/imagens/epitelial/b01-03.jpg',
            obs: ['Observar o limite celular em negro (impregnado pela prata)'],
          },
        ],
      },
      {
        name: 'B2',
        cards: [
          {
            name: 'B2 - Células descamadas do epitélio oral',
            image: '/imagens/epitelial/b02-01.jpg',
            obs: ['Núcleo central', 'Citoplasma periférico homogêneo'],
          },
        ],
      },
      {
        name: 'B3',
        cards: [
          {
            name: 'B3 - Pele, planta do pé - HE',
            image: '/imagens/epitelial/b03-01.jpg',
            obs: [
              '(1) Epitélio estratificado pavimentoso queratinizado',
              '(2) Queratina espessa',
            ],
          },
          {
            name: 'Médio aumento',
            image: '/imagens/epitelial/b03-02.jpg',
            obs: [
              '(1) Epitélio estratificado pavimentoso queratinizado',
              '(2) Queratina espessa',
            ],
          },
          {
            name: 'Maior aumento',
            image: '/imagens/epitelial/b03-03.jpg',
            obs: null,
          },
          {
            name: 'Epitélio glandular exócrino',
            image: '/imagens/epitelial/b03-05.jpg',
            obs: ['glândula sudorípara'],
          },
          {
            name: 'Porção secretora e excretora',
            image: '/imagens/epitelial/b03-04.jpg',
            obs: [
              '(Seta curta) - Porção secretora',
              '(Seta longa) - Porção excretora',
            ],
          },
        ],
      },
    ],
  }),

  actions: {},

  mutations: {},

  getters: {
    getItemByName: (state) => (name) => {
      return state.items.find((item) => item.name === name)
    },
  },
}
